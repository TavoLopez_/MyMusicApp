//
//  MyMusicAppApp.swift
//  MyMusicApp
//
//  Created by Gustavo.Ospina on 13/09/21.
//

import SwiftUI

@main
struct MyMusicAppApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
